from pybrain.datasets import SupervisedDataSet
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
import matplotlib.pyplot as plt
import numpy as np
from math import pow

input_size = 4

def readData(input_size):
    data = []
    file = open("data.csv")
    i = 0
    input = []
    for row in file:
        if i < input_size:
            input.append(float(row))
            i += 1
        else:
            i = 0
            data.append([input, row])
            input = []
    file.close()
    return data           

def writeResults(data):
    file = open("result.csv", "w")
    for [real, predicted] in data:
        file.write(str(real))
        file.write(",")
        file.write(str(predicted))
        file.write("\n")
    file.close()

def train():
    net = buildNetwork(input_size, 2, 1, bias=True)
    data = readData(input_size)
    ds = SupervisedDataSet(input_size, 1)
    result = []
    real = []
    predicted = []
    deviation = []
    for d in data:
        ds.clear()
        ds.addSample(d[0], d[1])
        predicted_value = float(net.activate(d[0])[0])
        trainer = BackpropTrainer(net, ds)
        trainer.trainEpochs(epochs=10)
        real_value = float(d[1])
        deviation.append(pow((predicted_value - real_value), 2))
        real.append(real_value)
        predicted.append(predicted_value)
        result.append([real_value, predicted_value])
    writeResults(result)
    indexs = np.arange(len(result))    
    plt.plot(indexs, real, 'r', indexs, predicted, 'b')
    plt.show()
    plt.plot(indexs, deviation)
    plt.show()
 
train()